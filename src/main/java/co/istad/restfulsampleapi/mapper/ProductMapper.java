package co.istad.restfulsampleapi.mapper;


import co.istad.restfulsampleapi.dto.ProductRequest;
import co.istad.restfulsampleapi.dto.ProductResponse;
import co.istad.restfulsampleapi.model.Product;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductMapper {
    ProductResponse maptoProductResponse(Product product);
    Product mapRequestToProduct(ProductRequest request);

//    @AfterMapping
//   default void addCategoryForResponse(Product product,ProductResponse productResponse){
//        if(productResponse.category()==null){
//
//        }
//    }

}
