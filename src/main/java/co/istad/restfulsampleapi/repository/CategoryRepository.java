package co.istad.restfulsampleapi.repository;


import co.istad.restfulsampleapi.model.Category;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CategoryRepository {
    private List<Category> categories = new ArrayList<>(){{
        add(new Category(1,"Food","Something you can eat"));
        add(new Category(2,"Electronics","Something electrical"));
    }};

    public List<Category> getAllCategories(){
        return categories;
    }

    public  void createCategory(Category category){
        categories.add(category);
    }
}
