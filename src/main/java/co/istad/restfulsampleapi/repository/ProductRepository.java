package co.istad.restfulsampleapi.repository;


import co.istad.restfulsampleapi.model.Product;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ProductRepository {
    private List<Product> products = new ArrayList<>(){{
        add(new Product(1,"Cocacola","Cool drink ",5.6f,"cocacolaimage.png",1));
        add(new Product(2,"Hatari","Electrical fan!",30f,"fanimage.png",2));
    }};

    public List<Product> getProducts() {
        return products;
    }


}
