package co.istad.restfulsampleapi.dto;


import lombok.Builder;

@Builder
public record CategoryRequest(String name, String description) {
}
