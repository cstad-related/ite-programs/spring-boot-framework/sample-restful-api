package co.istad.restfulsampleapi.dto;


import lombok.Builder;

@Builder
public record CategoryResponse(
        int id,
        String name,
        String description) {
}
