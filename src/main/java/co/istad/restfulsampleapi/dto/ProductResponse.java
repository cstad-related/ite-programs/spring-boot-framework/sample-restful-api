package co.istad.restfulsampleapi.dto;

import lombok.Builder;

@Builder
public record ProductResponse(int id ,
                              String name,
                              String description ,
                              String imageUrl ,
                              float price,
                              CategoryResponse category) {
}
