package co.istad.restfulsampleapi.dto;

import lombok.Builder;

@Builder
public record ProductRequest(String name,
                             String description ,
                             float price ,
                             String imageUrl ,
                             int categoryId) {
}
