package co.istad.restfulsampleapi.service.serviceImpl;

import co.istad.restfulsampleapi.dto.CategoryRequest;
import co.istad.restfulsampleapi.dto.ProductResponse;
import co.istad.restfulsampleapi.mapper.ProductMapper;
import co.istad.restfulsampleapi.model.Product;
import co.istad.restfulsampleapi.repository.CategoryRepository;
import co.istad.restfulsampleapi.repository.ProductRepository;
import co.istad.restfulsampleapi.service.ProductService;
import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;

import java.util.List;


@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;
    private final ProductMapper productMapper;

    @Override
    public List<ProductResponse> getAllProducts() {
        // get the category of each product

        return productRepository.getProducts()
                .stream()
                .map(productMapper::maptoProductResponse)
                .toList();

    }
}
