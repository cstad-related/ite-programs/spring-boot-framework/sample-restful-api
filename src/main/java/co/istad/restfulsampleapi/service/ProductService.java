package co.istad.restfulsampleapi.service;

import co.istad.restfulsampleapi.dto.ProductResponse;

import java.util.List;

public interface ProductService {
    List<ProductResponse> getAllProducts();
}
