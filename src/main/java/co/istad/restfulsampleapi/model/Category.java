package co.istad.restfulsampleapi.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Builder
@AllArgsConstructor
@Data
public class Category {
    private int id;
    private String name;
    private String description;
}
