package co.istad.restfulsampleapi.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@AllArgsConstructor
@Builder
@Data
public class Product {
    private int id;
    private String name;
    private String description;
    private float price;
    private String imageUrl;
    private int categoryId;
}
