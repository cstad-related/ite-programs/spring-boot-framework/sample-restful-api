package co.istad.restfulsampleapi.restcontroller;
import co.istad.restfulsampleapi.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.HashMap;

@RestController
@RequestMapping("api/v1/products")
@RequiredArgsConstructor
public class ProductRestController {

    private final ProductService productService;
    @GetMapping
    public HashMap<String , Object> getAllProduct(){
        HashMap<String, Object> response = new HashMap<>();
        response.put("payload",productService.getAllProducts());
        response.put("message","Get All The Product successfully!");
        response.put("status", HttpStatus.OK.value());
        return response;
    }
}
